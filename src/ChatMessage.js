import React from 'react'

export default ({ name, message }) =>
  <div className="row message-bubble">
    <strong>{name}</strong>
	<p>
  		<em className="message">{message}</em>
  	</p> 
  </div>

