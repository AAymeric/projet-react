import React from 'react';

export class Waypoints extends React.Component {

    constructor(props){
        super(props)
    }


    render(){

        const waypoints = this.props.waypoints;
        return (
            <div class="mb_2">
                <p>Cliquer sur un lieu pour obtenir son emplacement sur la carte, le film sera également resynchronisé : </p>
                {waypoints.map((value, index) => {
                    return <button key={index} onClick={() => this.props.onTimestampChange(value)}>{value.label}</button>
                })}
            </div>
        ) 
    }
}