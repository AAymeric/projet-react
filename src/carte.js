import React from 'react';
import PropTypes from "prop-types";

import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';


export class Carte extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
		var coords = this.props.coords;
		var label = this.props.label;

    	return (
			<div>
				<LeafletMap ref="map" id="mapid" center={coords} zoom={13}>
					<TileLayer
					attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
					url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>
					<Marker position={coords}>
						<Popup>
							{label}
						</Popup>
					</Marker>
				</LeafletMap>
			</div>
        );
	}
}