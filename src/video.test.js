import React from 'react';
import { render, waitForElement, toHaveBeenCalledTimes, toHaveBeenCalledWith} from '@testing-library/react';
import {Video} from './video'

beforeAll(() =>{
    jest.spyOn(global, 'fetch').mockImplementation(() => {
        return Promise.resolve({
            status: 200,
            json: () => {
                return Promise.resolve([
                    {file_url: "file_url"}
                ]);
            }
        });
    });

})

afterAll(()=>{
    fetch.mockClear();
});

test("renders without crashing", () => {
    const div = document.createElement("div");
    render(<Video />, div);
   }); 

test("backend is called", () => {
    render(<Video/>);
    expect(global.fetch).toHaveBeenCalledWith("https://imr3-react.herokuapp.com/backend");
})