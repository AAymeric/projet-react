import React from 'react';

export class Chapters extends React.Component {

    constructor(props){
        super(props)
    }

    

    render(){

        const chapters = this.props.chapters;
        return (
            <div class="mb_2">
                <p>Cliquer sur les boutons pour vous déplacer vers le chapitre souhaité : </p>
                {chapters.map((value, index) => {
                    return <button key={index} onClick={() => this.props.onPosChange(value.pos)}>{value.title}</button>
                })}
            </div>
  )
        
        
    }
}