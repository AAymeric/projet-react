import React from 'react';

export class Keywords extends React.Component {

    constructor(props){
        super(props)
    }

    

    render(){

        const printedKeywords = this.props.printedKeywords;
        return (
            <div>
                <p>Si dessous des infos sur la scène actuellement jouée : </p>
                <ul>
                {printedKeywords.map((value, index) => {
                return <li key={index}>{value.title} : <a href={value.url}>{value.url}</a></li>
                })}
                </ul>
            </div>
        )
    }
}