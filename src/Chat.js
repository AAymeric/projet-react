import React, { Component } from 'react'
import logo from './logo.svg'
import ChatInput from './ChatInput'
import ChatMessage from './ChatMessage'

const URL = "wss://imr3-react.herokuapp.com";

export class Chat extends Component {
  constructor(props){
    super(props)
    this.state = {
      name: '',
      messages: [],
    }
  }

  ws = new WebSocket(URL)

  componentDidMount() {

    this.ws.onopen = () => {
      console.log("connected");
      this.setState({
          connected: true
      });
    };

    this.ws.onmessage = evt => {
      const messages = JSON.parse(evt.data);
      messages.map(message => this.addMessage(message));
    };

    this.ws.onclose = () => {
        console.log("disconnected, reconnect.");
        this.setState({
            connected: false,
            ws: new WebSocket(URL)
        });
    };
  }

  addMessage = message =>
    this.setState(state => ({ messages: [message, ...state.messages] }))

  submitMessage = messageString => {
    const message = { name: this.state.name, message: messageString };
    this.ws.send(JSON.stringify(message));
    console.log("Message envoyé :" + JSON.stringify(message))
  }

  render() {
    if (this.state.connected === true) {
      return (
        <div className="container chat-container ">
          <div className="row">
            <div className="panel panel-default">
              <div className="hundred">
                <div className="panel-heading" id="titre"> CHAT </div>

{/*                                 Input pour le pseudo                                 */}
                  <div>
                    <label htmlFor="name">
                      Pseudo : &nbsp;
                      <input
                        type="text"
                        id={'name'}
                        placeholder={'Enter your pseudo ...'}
                        value={this.state.name}
                        onChange={e => this.setState({ name: e.target.value })}
                      />
                    </label>
                  </div>
                  <div className="panel-body">

{/*                               Affichage des messages                               */}
                    <div className="message-container" id="chat">       
                      {this.state.messages.map((message, index) =>
                        <ChatMessage
                          key={index}
                          message={message.message}
                          name={message.name}
                        />,
                      ).reverse()}
                    </div> 

{/*                                 Input pour le message                                 */}
                    <div> 
                      <ChatInput
                        ws={this.ws}
                        onSubmitMessage={messageString => this.submitMessage(messageString)}
                      />
                    </div>
                  </div>
                </div>  
              </div>
            </div>
          </div>
      )
    }
    else {
      return(
        <div className="chat-container">
        <p>
          Tentative de connection
          <img src={logo} className="App-logo" alt="logo" />
        </p>
        </div>
      )
    }
  }
}

export default Chat