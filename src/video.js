import React from 'react';
import "../node_modules/video-react/dist/video-react.css"; // import css
import { Player } from 'video-react';
import { Chapters } from "./chapters";
import { Carte } from "./carte";
import { Waypoints } from "./waypoints";
import { Keywords } from "./keywords";

export class Video extends React.Component {
  
  constructor(props){
    super(props)
    this.state = {
        src:"",
        coords: [32.42, -90.13], //Les coordonnées par défaut sont celles du début du film, c'est à dire de "Ridgeland"
        label: "Ridgeland",  //Le label par défaut est celui du début du film, c'est à dire de "Ridgeland"
        chapters: [],
        waypoints: [],
        keywords: [],
        printedKeywords: [
          {
            "title": "Route 66",
            "url": "https://en.wikipedia.org/wiki/U.S._Route_66"
          },
          {
            "title": "Stefan Kluge",
            "url": "http://www.imdb.com/name/nm1667631/"
          },
          {
            "title": "Mathias Einmann",
            "url": "http://www.imdb.com/name/nm1667578/"
          }
        ] //Les infos par défaut sont celles du début du film
      }
  }

  //A chaque fois que l'on appuie sur un bouton, on appelle cette fonction pour mettre à jour les infos
  ModifKeyword = () => {
    const { player } = this.player.getState();
    let keywords =  this.state.keywords;
    for(let i = 0; i < keywords.length; i++){
      if(player.currentTime <= keywords[i].pos){
        this.setState({printedKeywords: keywords[i].data})
        break;
      }
    }
  }


  render(){
    return (
      <div class="video_root">
        <Player
          ref={player => {
            this.player = player;
          }}
          playsInline
          poster="/assets/poster.png"
          src={this.state.src}
          >
        </Player>
        <Chapters onPosChange={(value) => {this.player.seek(value); this.ModifKeyword(); }} chapters={this.state.chapters}></Chapters>
        <Carte coords={this.state.coords} label={this.state.label} />
        <Waypoints onTimestampChange={(value) => {this.player.seek(value.timestamp); this.setState({coords: [value.lat, value.lng]}); this.setState({label: value.label}); this.ModifKeyword(); }} waypoints={this.state.waypoints}></Waypoints>
        <Keywords printedKeywords={this.state.printedKeywords} />
      </div>
    );
  }

  componentDidMount(){
    fetch("https://imr3-react.herokuapp.com/backend")
    .then(res => res.json())
    .then(result => {
      this.setState({
          src: result.Film.file_url,
          chapters: result.Chapters,
          waypoints: result.Waypoints,
          keywords: result.Keywords
      })
    })
  }
}
