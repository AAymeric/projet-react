import React, { Component } from 'react'
import './App.css'
import { Chat } from './Chat'
import { Video } from "./video"

class App extends Component {
  render() {
    return (

      <div className="App">
        <Video />
        <Chat />
      </div>
    )
  }
}

export default App